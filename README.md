# Pupp.rb

This is a very simple puppeteer wrapper for ruby. It supports only modifying a very limited subset.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'pupprb'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install pupprb

Also make sure puppeteer is installed:

    $ yarn add puppeteer

or

    $ npm i puppeteer

### Server installation

When running puppeteer on a server, you typically want to run it contained. To do this CHROME_DEVEL_SANDBOX needs to be set. If it isn't, the gem automatically tries:

* /usr/local/sbin/chrome-devel-sandbox
* /usr/local/sbin/chrome_sandbox

It also may require some other dependencies, for Debian I had to install the following:

    sudo apt-get install libatk1.0-0 libatk-bridge2.0-0 libgbm1 libxkbcommon0

## Usage

When setup correctly, printing a pdf can be as simple as:

    pdf_file = Pupprb::Pdf.write("http://example.com")

Pageoptions can be passed:

    pdf_file = Pupprb::Pdf.write("http://example.com", {page: {format: "A4", margin: {left: "0cm", top: "0cm", right: "0cm", bottom: "0cm"}})



## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/murb/pupprb. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/pupprb/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Pupprb project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/pupprb/blob/main/CODE_OF_CONDUCT.md).
