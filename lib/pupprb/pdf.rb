class Pupprb::Pdf
  class Unsecure < StandardError

  end
  class << self
    BASE58_ALPHABET = ("0".."9").to_a + ("A".."Z").to_a + ("a".."z").to_a - ["0", "O", "I", "l"]

    def base58(n = 16)
      SecureRandom.random_bytes(n).unpack("C*").map do |byte|
        idx = byte % 64
        idx = SecureRandom.random_number(58) if idx >= 58
        BASE58_ALPHABET[idx]
      end.join
    end

    def rails?
      defined?(Rails) && Rails.class.is_a?(Module) && Rails.methods.include?(:root)
    end

    def gem_root
      File.join(File.expand_path(File.dirname(__FILE__)), "../..")
    end

    def debug message
      rails? ? Rails.logger.debug(message) : puts(message)
    end

    def write(url, options = {})
      filename = rails? ? Rails.root.join("tmp/#{base58(32)}.pdf").to_s : "/tmp/#{base58(32)}.pdf"

      resource = clean_resource(url.to_s)

      exec_prefix = nvm? ? nvm_exec_path : nil

      command = [exec_prefix, File.join(gem_root, "bin", "puppeteer"), resource, filename].compact
      command << "--pageOptions=#{options[:page].to_json}" if options[:page]

      env = {}
      env["CHROME_DEVEL_SANDBOX"] = "/usr/local/sbin/chrome-devel-sandbox" if File.exist?("/usr/local/sbin/chrome-devel-sandbox")
      env["CHROME_DEVEL_SANDBOX"] ||= "/usr/local/sbin/chrome_sandbox" if File.exist?("/usr/local/sbin/chrome_sandbox")
      env["NODE_PATH"] = Rails.root.join("node_modules").to_s if rails?


      if !node_present?
        raise "Node not found. Required."
      end

      debug("Start creating a pdf using puppeteer, command: #{env.map{|k,v| "#{k}=>#{v}"}} #{command.join(' ')}")
      system(env, *command, exception: true)

      return filename
    end

    private

    def nvm?
      File.exist?(nvm_exec_path)
    end

    def nvm_exec_path
      File.expand_path("~/.nvm/nvm-exec")
    end

    def node_present?
      !!system("node --version")
    end

    def clean_resource(url)
      # urls are recognized as urls, but local files are not; simple trick that works on unixy systems
      if /\A\/tmp\/[A-Za-z\d.\/]*/.match?(url)
        "file://#{url}"
      elsif rails? && /\A#{Rails.root}\/tmp\/[A-Za-z\d.\/]*/.match?(url)
          "file://#{url}"
      elsif rails? && url.start_with?(File.join(Rails.root, "public"))
        "file://#{url}"
      elsif valid_rails_url_starts.find{|valid_base_url| url.start_with?(valid_base_url)}
        url
      else
        raise Unsecure.new("Unsecure location (#{url})")
      end
    end

    def valid_rails_url_starts
      urls = []
      return urls if !rails?

      if Rails.env.test? || Rails.env.development?
        urls << ActionDispatch::Http::URL.full_url_for(Rails.application.config.action_mailer.default_url_options.merge(secure: false, protocol: :http))
      end
      urls << ActionDispatch::Http::URL.full_url_for(Rails.application.config.action_mailer.default_url_options.merge(secure: true, protocol: :https))

      Rails.configuration.hosts.each do |host|
        if Rails.env.test? || Rails.env.development?
          urls << ActionDispatch::Http::URL.full_url_for(host: host.to_s, port: 3000, secure: false)
          urls << ActionDispatch::Http::URL.full_url_for(host: host.to_s, secure: false)
        end
        urls << ActionDispatch::Http::URL.full_url_for(host: host.to_s, secure: true, protocol: :https)
      end
      urls
    end
  end
end