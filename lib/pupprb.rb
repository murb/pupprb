# frozen_string_literal: true

require_relative "pupprb/version"
require_relative "pupprb/pdf"

module Pupprb
  class Error < StandardError; end
  # Your code goes here...
end
