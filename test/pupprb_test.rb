# frozen_string_literal: true

require "test_helper"

class PupprbTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Pupprb::VERSION
  end

  def test_it_raises_unsecure_error_when_unknown_location_is_used
    assert_raises(::Pupprb::Pdf::Unsecure) do
      ::Pupprb::Pdf.write("http://example.com")
    end
  end

  def test_it_renders_a_pdf_when_location_is_approved
    ::Pupprb::Pdf.stub :valid_rails_url_starts, ["http://example.com"] do
      ::Pupprb::Pdf.stub(:system, true) do
        ::Pupprb::Pdf.stub(:base58, "abc") do
          assert_equal("/tmp/abc.pdf", ::Pupprb::Pdf.write("http://example.com"))
        end
      end
    end
  end
end
